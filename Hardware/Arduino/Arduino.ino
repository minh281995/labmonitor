#include "DHT.h" 
#include <ArduinoJson.h>
#include <SoftwareSerial.h>

#define debugPrint Serial.print
#define debugPrintln Serial.println

#define NO_DUST_VOLTAGE 400 //400 //mv 
#define SYS_VOLTAGE 5000 
#define COV_RATIO 0.2 //ug/mmm / mv
#define ledPin 13

/* ESP8266 WiFi*/
SoftwareSerial wifi(6, 5);// RX 10-TX 9 nối với TX-RXcủa LoRa


/* Temperature & Humidity sensor*/
const int DHTPIN = 7;
const int DHTTYPE = DHT11;
DHT dht(DHTPIN, DHTTYPE);

//value of temperature & humdity
int hum;
int temp;

/* H2S MQ5 sensor */
const int DOUT_H2S = 9;
const int AOUT_H2S = 3;

// value of H2S
int valueH2S;

/* Dust PM2.5 sensor */
//pin 1 connect Cap, Res into VCC = 5V
//pin 2 & 4 connect CAP then connect GND
//pin 3 connect pin D9
//pin 5 connect A0
//pin 6 coonect VCC = 5V

const int measurePin_PM25 = A2;//A0
const int ledPower = 10; //D9 //const int ledPower = 8; 
 
int samplingTime = 280;
int deltaTime = 40;
int sleepTime = 9680;

// Initialize value of PM2.5
int voMeasured;
float voltage;
float dustDensity;

/* CO Sensor MQ7*/
const int DOUT_CO=8; //the DOUT pin of the CO sensor goes into digital pin D8 of the arduino
const int AOUT_CO=1; //the AOUT pin of the CO sensor goes into analog pin A1 of the arduino

//value of CO
int valueCO;
int limit;

/* SO2 Sensor MQ136*/
const int DOUT_SO2 = 4;
const int AOUT_SO2 = 0; 

//value of SO2
int valueSO2;

/* Json format */
const int BUFFER_SIZE = JSON_OBJECT_SIZE(6) + JSON_ARRAY_SIZE(0);
char buffer[BUFFER_SIZE];


//Filter function for PM 2.5
float filter(float m)
{
  static float flag_first = 0, _buff[10], sum;
  const float _buff_max = 10;
  int i;
  
  if(flag_first == 0)
  {
    flag_first = 1;

    for(i = 0, sum = 0; i < _buff_max; i++)
    {
      _buff[i] = m;
      sum += _buff[i];
    }
    return m;
  }
  else
  {
    sum -= _buff[0];
    for(i = 0; i < (_buff_max - 1); i++)
    {
      _buff[i] = _buff[i + 1];
    }
    _buff[9] = m;
    sum += _buff[9];
    
    i = sum / 10.0;
    return i;
  }
}

void setupWiFi() {

  wifi.begin(115200);
}

void setupSensors()
{
  dht.begin();
  pinMode(ledPower,OUTPUT);// pm2.5 
  digitalWrite(ledPower, LOW); //iled default closed
  pinMode(DOUT_CO, INPUT);//
  pinMode(ledPin, OUTPUT);//sets the pin as an output of the arduino
}

float getDustDensity()
{
  /* Read PM2.5 sensor */
  digitalWrite(ledPower,HIGH); // power on the LED
  delayMicroseconds(samplingTime);
  voMeasured = analogRead(measurePin_PM25); // read the dust value
  delayMicroseconds(deltaTime);
  digitalWrite(ledPower,LOW); // turn the LED off
  delayMicroseconds(sleepTime);
  // String data=String(voMeasured);
  // debugPrint("get data sensor :");
  // debugPrintln(data);

  //filter data from PM2.5 sensor
  voMeasured = filter(voMeasured);  

  // read data from PM2.5 sensor
  for(int i=0; i<10; i++){
    delay(100);
    digitalWrite(ledPower,HIGH); // power on the LED
    delayMicroseconds(samplingTime);
    voMeasured = analogRead(measurePin_PM25); // read the dust value
    delayMicroseconds(deltaTime);
    digitalWrite(ledPower,LOW); // turn the LED off
    delayMicroseconds(sleepTime);
    voMeasured = filter(voMeasured);
  }

  // convert voltage (mv)
  voltage = (SYS_VOLTAGE / 1024.0) * voMeasured*11;

  // voltage to density
  if(voltage >= NO_DUST_VOLTAGE)
  {
    voltage -= NO_DUST_VOLTAGE;
    dustDensity = voltage *COV_RATIO;
  }
  else
    dustDensity = 0;

  return dustDensity;
}

// float getDustDensity(){
//  return dustDensity;
// }

int getValueCO(){
  int AvalueCO = analogRead(AOUT_CO); //read the analaog value from the CO sensor's AOUT pin
  float RsRo = (1024-AvalueCO)/AvalueCO;
  valueCO = pow((RsRo/22.073), (1/-0.66659));
  return (unsigned int) valueCO;
}

int getValueSO2(){
  int AvalueSO2 = analogRead(AOUT_SO2); //read the analaog value from the SO2 sensor's AOUT pin
  float RsRoSO2 =(1024-AvalueSO2)/AvalueSO2;
  valueSO2 = pow((RsRoSO2/22.073), (1/-0.66659));
  return (unsigned int) valueSO2;
}

int getValueH2S(){
  int AvalueH2S = analogRead(AOUT_H2S); //read the analaog value from the H2S sensor's AOUT pin
  float RsRoH2S =(1024-AvalueH2S)/AvalueH2S;
  valueH2S = pow((RsRoH2S/22.073), (1/-0.66659));
  return (unsigned int) valueH2S;
}

int getTempAndHum()
{
  /* Read Temperature & Humidty sensor*/
  hum = (int)dht.readHumidity();   //Read digital value from Humidity sensor
  temp = (int)dht.readTemperature(); //Read digital value from Temperature sensor
  return (unsigned int) temp, hum;
}

void transferData()
{
  StaticJsonBuffer<BUFFER_SIZE> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonObject& d = root.createNestedObject("d");
  d["temp"] = temp;
  d["hum"] = hum;
  d["co"] = valueCO;
  d["pm25"] = dustDensity;
  d["so2"] = valueSO2;
  d["h2s"] = valueH2S;


  root.printTo(buffer, sizeof(buffer));
  debugPrintln(buffer);

  wifi.println(buffer);
}

void setup() 
{
  Serial.begin(115200);
  setupWiFi();
  setupSensors();
  debugPrintln("SETUP OK!");
}

void loop()
{
  getDustDensity();
  getValueCO();
  getValueSO2();
  getValueH2S();
  getTempAndHum();
  transferData();
  delay(5000);
}


