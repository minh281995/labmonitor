var express = require('express');
var router = express.Router();

const Cloudant = require('cloudant');

const account = process.env.CLOUDANT_USERNAME;
const password = process.env.CLOUDANT_PASSWORD;

const cloudant = Cloudant({account, password, plugin:'promises'});

router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

router.get('/api/v1/records', function(req, res) {
  const db = cloudant.db.use('labmonitor');

  const defaultOptions = {
    selector: {
      timestamp: 
      {
        '$gt': 0
      }
    },
    limit: 10,
    sort: [
      {
        timestamp: 'desc'
      }
    ]
  };

  db.find(defaultOptions)
    .then(data => {
      res.json({
        status: 'success',
        data: data.docs
      });
    })
    .catch(err => {
      debug(err);
      res.json(err)
    });
});

// router.get('/api/v1/nodes/:node', function(req, res) {
//   const db = cloudant.db.use('tracking');

//   const options = {
//     "selector": {
//       "node": {
//         "$eq": req.params.node
//       }
//     },
//     "sort": [
//       {
//         "timestamp": "desc"
//       }
//     ],
//     "limit": 5
//   }

//   db.find(options)
//     .then(data => {
//       res.json({
//         status: 'success',
//         data: data.docs
//       });
//     })
//     .catch(err => {
//       debug(err);
//       res.json(err)
//     });
// });

module.exports = router;
