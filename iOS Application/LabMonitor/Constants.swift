//
//  Constants.swift
//  LabMonitor
//
//  Created by Anh Phan Tran on 3/1/17.
//  Copyright © 2017 The Distance. All rights reserved.
//

import Foundation
import EZSwiftExtensions

struct API {
    static let baseURL = "https://labmonitor.mybluemix.net"
    static let version = "v1"
}

struct Colors {
    static let lightGray = UIColor(hexString: "ECF0F1")!
    static let darkGray = UIColor(hexString: "bdc3c7")!
    static let moreGray = UIColor(hexString: "95A5A6")!
    static let deepBlue = UIColor(hexString: "2C3E50")!
    static let lessBlue = UIColor(hexString: "34495e")!
}

struct Settings {
    static let refreshRate: Int = 30
}
