//
//  LoadingView.swift
//  LabMonitor
//
//  Created by Anh Phan Tran on 3/1/17.
//  Copyright © 2017 The Distance. All rights reserved.
//

import Foundation
import UIKit

class LoadingView {
    internal static var backgroundView: UIView = {
        let bgView = UIView()
        bgView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        bgView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
        bgView.isUserInteractionEnabled = false
        
        let view = UIView()
        view.backgroundColor = UIColor.darkGray
        let position = CGPoint(x: (bgView.bounds.size.width / 2.0) - 50.0, y: (bgView.bounds.size.height / 2.0) - 50.0)
        view.frame = CGRect(x:position.x, y:position.y, width: 100.0, height: 100.0)
        view.layer.cornerRadius = 7.0
        
        view.addSubview(activityView)
        let activityPosition = CGPoint(x: (view.bounds.size.width / 2.0) - (activityView.bounds.size.width / 2.0), y: (view.bounds.size.height / 2.0) - (activityView.bounds.size.height / 2.0))
        activityView.frame = CGRect(origin: activityPosition, size: CGSize(width: activityView.bounds.size.width, height: activityView.bounds.size.height))
        bgView.addSubview(view)
        return bgView
    }()
    
    
    internal static var activityView: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    static func show(in view: UIView) {
        DispatchQueue.main.async {
            self.backgroundView.removeFromSuperview()
            self.activityView.stopAnimating()
            view.addSubview(self.backgroundView)
            self.backgroundView.center = view.center
            self.activityView.startAnimating()
        }
    }
    
    static func dismissFromView() {
        DispatchQueue.main.async {
            self.backgroundView.removeFromSuperview()
            self.activityView.stopAnimating()
        }
    }
    
}
