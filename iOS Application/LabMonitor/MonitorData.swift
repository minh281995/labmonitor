//
//  Data.swift
//  LabMonitor
//
//  Created by Anh Phan Tran on 3/1/17.
//  Copyright © 2017 The Distance. All rights reserved.
//

import Foundation

class MonitorRecord {
    var so2: Int
    var co: Int
    var pm25: Int
    var temp: Int
    var hum: Int
    var h2s: Int
    var updatedAt: Date
    
    init(so2: Int, co: Int, pm25: Int, temp: Int, hum: Int, h2s: Int, timeStamp: TimeInterval) {
        self.so2 = so2
        self.co = co
        self.pm25 = pm25
        self.temp = temp
        self.hum = hum
        self.h2s = h2s
        self.updatedAt = Date(timeIntervalSince1970: timeStamp)
    }
}
