//
//  Router.swift
//  LabMonitor
//
//  Created by Anh Phan Tran on 3/1/17.
//  Copyright © 2017 The Distance. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum Router: URLRequestConvertible {
    static let baseURLString = API.baseURL
    
    // cases
    case refresh
    
    // methods:
    var method: Alamofire.HTTPMethod {
        switch self {
        case .refresh:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .refresh:
            return "/api/" + API.version + "/records"
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .refresh:
            return nil
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: Router.baseURLString)!
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        urlRequest.httpMethod = method.rawValue
        
        if let params = self.parameters {
            print("request with theses parameters : \(params)")
            return try URLEncoding.default.encode(urlRequest, with: params).urlRequest!
        }
        
        return urlRequest
    }
    
    func performRequest(completionHandler: @escaping ((JSON?, AppError?) -> Void)) {
        request(self).responseData(completionHandler: {(response) -> Void in
            switch response.result {
            case .failure(let error):
                print("ERROR : \(error.localizedDescription)")
                completionHandler(nil, AppError.unknown)
                return
            case .success(_):
                switch response.response!.statusCode
                {
                case 200, 422:
                break // will be handle in responseJSON
                case 401:
                    // This can be wrong credentials
                    switch self {
                    case .refresh:
                        completionHandler(nil, AppError.sessionInvalid)
                    }
                case 500:
                    completionHandler(nil, AppError.serverError)
                default:
                    print("WARNING : status code \(response.response!.statusCode) not handled")
                    completionHandler(nil, AppError.unknown)
                }
            }
        }).responseJSON(completionHandler: {(response) -> Void in
            switch response.result {
            case .failure(let error):
                print("ERROR : \(error.localizedDescription)")
            case .success(let json):
                switch response.response!.statusCode {
                case 200:
                    switch self {
                    case .refresh:
                        let jsonObj = JSON(json)
                        
                        guard let status = jsonObj["status"].string, status == "success" else {
                            completionHandler(nil, AppError.serverError)
                            return
                        }
                        
                        completionHandler(jsonObj, nil)
                    }
                case 422: //error in json
                    print(json)
                    completionHandler(nil, AppError.invalidParameters("in json"))
                default:
                    print("WARNING : status code \(response.response!.statusCode) not handled")
                }
            }
        })
    }
}
