//
//  MonitorRecordLineChartView.swift
//  LabMonitor
//
//  Created by Anh Phan Tran on 3/1/17.
//  Copyright © 2017 The Distance. All rights reserved.
//

import Foundation
import Charts
import EZSwiftExtensions

enum RecordChartViewState: String {
    case so2
    case hum
    case temp
    case pm25
    case h2s
    case co
}

class AxisDateFormatter: NSObject, IAxisValueFormatter {
    var referrencedDate: Date
    
    init(refDate: Date) {
        self.referrencedDate = refDate
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let currentDate = Date(timeInterval: value, since: self.referrencedDate)
        
        if currentDate.daysInBetweenDate(referrencedDate) >= 1 {
            return currentDate.toLocalString(format: "HH:mm-dd/MM")
        } else {
            return currentDate.toLocalString(format: "HH:mm")
        }
    }
}

protocol MonitorRecordLineChartViewDelegate {
    func chart(_ chart: MonitorRecordLineChartView, didChangeStateTo state: RecordChartViewState)
}

extension Date {
    func toLocalString(format: String) -> String {
        // change to a readable time format and change to local time zone
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = TimeZone.current
        let timeStamp = formatter.string(from: self)
        
        return timeStamp
    }
}

class MonitorRecordLineChartView: UIView {
    // MARK: - IBOutlet:
    @IBOutlet weak var lineChartView: LineChartView!
    
    // MARK: - Model:
    var datasets = [String: LineChartDataSet]()
    var currentState = RecordChartViewState.temp {
        didSet {
            self.delegate?.chart(self, didChangeStateTo: currentState)
        }
    }
    var delegate: MonitorRecordLineChartViewDelegate?
    
    // MARK: - Func:
    func updateDataset(with records: [MonitorRecord]) {
        let tempDataset = LineChartDataSet()
        let coDataset = LineChartDataSet()
        let so2Dataset = LineChartDataSet()
        let humDataset = LineChartDataSet()
        let pm25Dataset = LineChartDataSet()
        let h2sDataset = LineChartDataSet()
        
        for record in records {
            let timeStamp = record.updatedAt.timeIntervalSince(records[0].updatedAt)
            
            // temp
            let tempDataEntry = ChartDataEntry(x: timeStamp, y: Double(record.temp))
            _ = tempDataset.addEntry(tempDataEntry)
            
            // co
            let coDataEntry = ChartDataEntry(x: timeStamp, y: Double(record.temp))
            _ = coDataset.addEntry(coDataEntry)
            
            // so2
            let so2DataEntry = ChartDataEntry(x: timeStamp, y: Double(record.so2))
            _ = so2Dataset.addEntry(so2DataEntry)
            
            // pm25
            let pm25DataEntry = ChartDataEntry(x: timeStamp, y: Double(record.pm25))
            _ = pm25Dataset.addEntry(pm25DataEntry)
            
            // h2s
            let h2sDataEntry = ChartDataEntry(x: timeStamp, y: Double(record.h2s))
            _ = h2sDataset.addEntry(h2sDataEntry)
            
            // hum
            let humDataEntry = ChartDataEntry(x: timeStamp, y: Double(record.hum))
            _ = humDataset.addEntry(humDataEntry)
        }
        
        if records.count > 0 {
            self.lineChartView.xAxis.valueFormatter = AxisDateFormatter(refDate: records[0].updatedAt)
        } else {
            self.lineChartView.xAxis.valueFormatter = nil
        }
        
        self.datasets = [
            "temp": tempDataset,
            "co": coDataset,
            "so2": so2Dataset,
            "hum": humDataset,
            "pm25": pm25Dataset,
            "h2s": h2sDataset,
        ]
    }
    
    func drawChart() {
        if let currentDataset = self.datasets[self.currentState.rawValue] {
            currentDataset.drawCirclesEnabled = false
            currentDataset.colors = [Colors.lessBlue]
            self.lineChartView.data = LineChartData(dataSet: currentDataset)
            
            self.lineChartView.data?.setDrawValues(false)
            
            self.lineChartView.animate(xAxisDuration: 0.25, easingOption: ChartEasingOption.easeOutExpo)
        }
    }
    
    // MARK: - View Life Cycle:
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lineChartView.noDataText = "Data not ready"
        self.lineChartView.isUserInteractionEnabled = false
        self.lineChartView.legend.enabled = false
        self.lineChartView.rightAxis.enabled = false
        self.lineChartView.chartDescription?.text = ""
        self.lineChartView.xAxis.labelPosition = .bottom
        self.updateDataset(with: [])
    }
}
