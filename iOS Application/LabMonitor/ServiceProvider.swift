//
//  ServiceProvider.swift
//  LabMonitor
//
//  Created by Anh Phan Tran on 3/1/17.
//  Copyright © 2017 The Distance. All rights reserved.
//

import Foundation
import SwiftyJSON

typealias ServiceProviderCompletionBlock = ([MonitorRecord]?, AppError?) -> Void

class ServiceProvider {
    private init() {}
    
    static let shared = ServiceProvider()
    
    func refresh(completionHandler: ServiceProviderCompletionBlock?) {
        Router.refresh.performRequest { json, error in
            if error != nil {
                completionHandler?(nil, error)
            } else if let json = json {
                let data = json["data"].arrayValue
                
                var records = [MonitorRecord]()
                
                for item in data {
                    guard let co = item["co"].int,
                        let so2 = item["so2"].int,
                        let pm25 = item["pm25"].int,
                        let temp = item["temp"].int,
                        let hum = item["hum"].int,
                        let h2s = item["h2s"].int,
                        let milliSecTimeStamp = item["timestamp"].double else {
                            continue
                    }
                    
                    let timeStamp = (milliSecTimeStamp / 1000.0)
                    
                    let record = MonitorRecord(so2: so2, co: co, pm25: pm25, temp: temp, hum: hum, h2s: h2s, timeStamp: timeStamp)
                    
                    records.insert(record, at: 0)
                }
                
                completionHandler?(records, nil)
            } else {
                completionHandler?(nil, AppError.unknown)
            }
        }
        
        
    }
}
