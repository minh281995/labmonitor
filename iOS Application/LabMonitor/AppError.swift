//
//  AppError.swift
//  LabMonitor
//
//  Created by Anh Phan Tran on 3/1/17.
//  Copyright © 2017 The Distance. All rights reserved.
//

import Foundation

enum AppError: Error {
    case unknown
    case sessionInvalid
    case serverError
    case invalidParameters(String)
}

extension AppError {
    var failureReason: String {
        switch self {
        case .unknown:
            return "Unknown error occured"
        case .sessionInvalid:
            return "Session Invalid"
        case .serverError:
            return "Server Error"
        case .invalidParameters(let param):
            return "Some parameters are invalid: \(param)"
        }
    }
}
