//
//  MonitorButton.swift
//  LabMonitor
//
//  Created by Anh Phan Tran on 3/1/17.
//  Copyright © 2017 The Distance. All rights reserved.
//

import Foundation
import UIKit


class MonitorButton: UIView {
    // MARK: - IBOutlets:
    @IBOutlet weak var recordLabel: UILabel!
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Model:
    var action: (() -> Void)?
    var record: String! {
        didSet {
            self.recordLabel.text = record
        }
    }
    var selected: Bool = false {
        didSet {
            self.backgroundColor = selected ? Colors.lightGray: Colors.lessBlue
            self.recordLabel.textColor = selected ? Colors.lessBlue : Colors.lightGray
            self.unitLabel.textColor = selected ? Colors.lessBlue : Colors.lightGray
            self.iconImageView.tintColor = selected ? Colors.lessBlue : Colors.moreGray
            self.descriptionLabel.textColor = selected ? Colors.lessBlue : Colors.moreGray
        }
    }
    var icon: UIImage! {
        didSet {
            self.iconImageView.image = icon.withRenderingMode(.alwaysTemplate)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.record = "0"
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapHandler))
        self.addGestureRecognizer(tapGesture)
        self.selected = false
    }
    
    func tapHandler() {
        self.action?()
    }
}
