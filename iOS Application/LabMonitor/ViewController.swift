//
//  ViewController.swift
//  LabMonitor
//
//  Created by Anh Phan Tran on 3/1/17.
//  Copyright © 2017 The Distance. All rights reserved.
//

import UIKit
import Charts
import ReachabilitySwift
import EZSwiftExtensions

class ViewController: UIViewController {
    @IBOutlet weak var lineChartView: MonitorRecordLineChartView!
    @IBOutlet weak var coButton: MonitorButton!
    @IBOutlet weak var so2Button: MonitorButton!
    @IBOutlet weak var pm25Button: MonitorButton!
    @IBOutlet weak var humButton: MonitorButton!
    @IBOutlet weak var tempButton: MonitorButton!
    @IBOutlet weak var h2sButton: MonitorButton!
    @IBOutlet weak var lastUpdateLabel: UILabel!
    @IBOutlet weak var noInternetView: UIView!
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    @IBOutlet weak var countDownLabel: UILabel!
    
    @IBOutlet weak var noInternetViewHeightConstraint: NSLayoutConstraint!
    
    @IBAction func refresh() {
        LoadingView.show(in: self.view)
        self.refreshButton.isEnabled = false
        self.invalidateTimers()
        ServiceProvider.shared.refresh { records, error in
            LoadingView.dismissFromView()
            self.refreshButton.isEnabled = true
            if error != nil {
                let alertVC = UIAlertController(title: "Error", message: error?.failureReason, preferredStyle: .alert)
                
                alertVC.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                
                self.presentVC(alertVC)
            } else {
                self.lineChartView.updateDataset(with: records!)
                self.lineChartView.drawChart()
                
                if let latestRecord = records!.last {
                    self.lastUpdateLabel.text = "Last updated: \(latestRecord.updatedAt.toString(dateStyle: .short, timeStyle: .short))"
                    self.coButton.record = "\(latestRecord.co)"
                    self.so2Button.record = "\(latestRecord.so2)"
                    self.tempButton.record = "\(latestRecord.temp)"
                    self.pm25Button.record = "\(latestRecord.pm25)"
                    self.h2sButton.record = "\(latestRecord.h2s)"
                    self.humButton.record = "\(latestRecord.hum)"
                }
                
                self.refreshTimers()
            }
        }
    }
    
    // MARK: - Model:
    let reachability = Reachability()!
    var countDownTimer: Timer?
    var countDown: Int = Settings.refreshRate
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.setup()
        self.lineChartView.currentState = .temp
        self.countDownLabel.text = "\(Settings.refreshRate)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimers()
    }
    
    func invalidateTimers() {
        self.countDownTimer?.invalidate()
        self.countDownTimer = nil
    }
    
    func refreshTimers() {
        self.countDown = Settings.refreshRate
        
        self.countDownTimer = ez.runThisEvery(seconds: 1.0, startAfterSeconds: 0.0, handler: { _ in
            if self.countDown > 0 {
                self.countDown -= 1
                self.countDownLabel.text = "\(self.countDown)"
            } else {
                self.refresh()
            }
        })
    }
    
    func setup() {
        self.lineChartView.delegate = self
        
        self.coButton.action = {
            self.lineChartView.currentState = .co
            self.lineChartView.drawChart()
        }
        self.coButton.icon = #imageLiteral(resourceName: "metric")
        
        self.so2Button.action = {
            self.lineChartView.currentState = .so2
            self.lineChartView.drawChart()
        }
        self.so2Button.icon = #imageLiteral(resourceName: "metric")
        
        self.pm25Button.action = {
            self.lineChartView.currentState = .pm25
            self.lineChartView.drawChart()
        }
        self.pm25Button.icon = #imageLiteral(resourceName: "metric")
        
        self.humButton.action = {
            self.lineChartView.currentState = .hum
            self.lineChartView.drawChart()
        }
        self.humButton.icon = #imageLiteral(resourceName: "humid")
        
        self.tempButton.action = {
            self.lineChartView.currentState = .temp
            self.lineChartView.drawChart()
        }
        self.tempButton.icon = #imageLiteral(resourceName: "temp")
        
        self.h2sButton.action = {
            self.lineChartView.currentState = .h2s
            self.lineChartView.drawChart()
        }
        self.h2sButton.icon = #imageLiteral(resourceName: "metric")
        
        self.reachability.whenReachable = { reachability in
            DispatchQueue.main.async {
                self.refreshButton.isEnabled = true
                self.refresh()
                if self.noInternetViewHeightConstraint.constant != 0 {
                    
                    self.noInternetViewHeightConstraint.constant = 0.0
                    self.noInternetView.setNeedsLayout()
                    self.noInternetView.setNeedsUpdateConstraints()
                    
                    UIView.animate(withDuration: 0.25, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                        self.view.layoutIfNeeded()
                    }, completion: nil)
                }
            }
        }
        self.reachability.whenUnreachable = { reachability in
            DispatchQueue.main.async {
                if self.noInternetViewHeightConstraint.constant != 25.0 {
                    self.invalidateTimers()
                    self.refreshButton.isEnabled = false
                    self.noInternetViewHeightConstraint.constant = 25.0
                    self.noInternetView.setNeedsLayout()
                    self.noInternetView.setNeedsUpdateConstraints()
                    
                    UIView.animate(withDuration: 0.25, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                        self.view.layoutIfNeeded()
                    }, completion: nil)
                }
            }
        }
    }
}

extension ViewController: MonitorRecordLineChartViewDelegate {
    func chart(_ chart: MonitorRecordLineChartView, didChangeStateTo state: RecordChartViewState) {
        self.coButton.selected = state.rawValue == "co"
        self.so2Button.selected = state.rawValue == "so2"
        self.pm25Button.selected = state.rawValue == "pm25"
        self.tempButton.selected = state.rawValue == "temp"
        self.h2sButton.selected = state.rawValue == "h2s"
        self.humButton.selected = state.rawValue == "hum"
    }
}

